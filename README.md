# Schedule Planning System

## Core Features

System is packed with powerful functionalities to revolutionize the way you plan and collaborate:

- **Real-time Cursors**: See where your teammates are working in the document in real time.
- **Real-time Text Selection**: View selections made by others instantly, enhancing collaboration.
- **Real-time Database and Collaboration**: Built on a robust real-time database that supports simultaneous work without conflicts.
- **Real-time Presence**: Know who's online, active, or idle with our real-time presence feature.
- **Move to Trash Functionality**: Easily discard unwanted items with a simple move to trash option.
- **Custom Emoji Picker**: Express yourself with a wide range of custom emojis.
- **Light Mode/Dark Mode**: Choose between light and dark themes for optimal visual comfort.
- **Next.js 13 App Router**: Utilizing the latest Next.js features for efficient routing.
- **Creating Free Plan Restrictions**: Designed to offer flexibility with free plan restrictions for various user needs.
- **Take Monthly Payments**: Secure monthly payment processing to access premium features.
- **Custom Email 2FA Invitation**: Enhanced security with two-factor authentication via custom email invitations.
- **Supabase Row Level Policy**: Implement fine-grained access control with Supabase's row-level security.
- **Real-time Collaboration**: Work together in real-time, seeing changes and contributions instantaneously.

## Technologies Used

This project is built using modern technologies and services to ensure high performance and reliability:

- **Next.js**: For the frontend framework, utilizing the latest routing capabilities.
- **Supabase**: As the backend and real-time database, providing authentication, storage, and row-level security.
- **Stripe**: For handling monthly payments and subscription management.
- **Third-Party Email Provider**: For sending custom 2FA invitations and notifications.